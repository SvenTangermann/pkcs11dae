#!/usr/bin/env sh
for FILE in ./*.dot
do
    FILE="${FILE%.*}"
    dot "${FILE}.dot" -Tps -o "${FILE}.ps"
    echo "${FILE}.dot processed"
done
