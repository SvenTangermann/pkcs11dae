Proof lemmas are the holy grail in the world of models~--~right after tools which derive proof lemmas. 
They formalize the expected results and requirements.
Some lemmas need a custom proof strategy given by an oracle in 
\cref{sec:oracle}.

\begin{mslemma}[{Counter_Monotonicity[use_induction,reuse]}]
    States that a device counter increases between two usages.
    Is proven by induction because the lemma itself needs to be applied to earlier time points within the proof.
    \begin{minted}{spthy}
"All d c C #before #later . 
    DCtrIs(d,c)@before & DCtrIs(d,C)@later & #before<#later ==> 
        Ex z . C=c+z"
    \end{minted}
    %Is marked as reusable to be used to proof other lemmas.
    \label{lemma:ctr}
\end{mslemma}

\begin{mslemma}[IV_Uniqueness]
    Depends on \cref{lemma:ctr}.
    States that two encryptions performed on some devices 
    differ at least in the used initialization vector. 
    Even if this lemma would be violated all other lemmas could hold. 
    But since the same initialization vector results in the same key stream, 
    one could easily gain some information from two ciphertexts 
    which were created with the same initialization vector. 
    \begin{minted}{spthy}
"All iv #before #later . IV(iv)@before & IV(iv)@later ==> #later=#before"
    \end{minted}
\end{mslemma}

\begin{mslemma}[Key_UsageImpliesInitialization]
    States that whenever a key is used on a device 
    it must have been initialized before.
    On its own it seems not that useful, 
    but this lemma shows how actions can be used to describe the structure of a model:
    Whenever a key is used, we can refer to the point in time when it was initialized, 
    which allows reasoning about how it was initialized~--~either by creation or by import.
    \begin{minted}{spthy}
"All d k l #keyUse . UseKey(d,k,l)@keyUse ==>
     Ex #keyInit . InitKey(d,k,l)@keyInit & #keyInit<#keyUse"
    \end{minted}
\end{mslemma}

\begin{mslemma}[{Key_IntegrityAndConfidentiality[use_induction,reuse]}]
    States that all initialized keys were created on some device and are never known.
    Is proven by induction because whenever some key is known, 
    it is trivial to decrypt some wrapping created with this key 
    or to inject a Trojan key. 
    Is marked as reusable to be used to proof other lemmas.
    In fact every lemma from here on (excluding \cref{lemma:lowest}) depends on it.
    \begin{minted}{spthy}
" ( not Ex k l #keyCreate #keyKU . CreateKey(k,l)@keyCreate & KU(k)@keyKU )
& ( All d k l #keyImport . ImportKey(d,k,l)@keyImport ==>
        Ex #keyCreate . CreateKey(k,l)@keyCreate & #keyCreate<#keyImport )"
    \end{minted}
    \label{lemma:kic}
\end{mslemma}

\begin{mslemma}[Key_UniqueLevel]
    States that each key is bound to one level. 
    This chaining also shows the absence of \emph{Key Cycles}.
    \begin{minted}{spthy}
"All d D k l L #i #j . InitKey(d,k,l)@i & InitKey(D,k,L)@j ==> l=L"
    \end{minted}
\end{mslemma}

\begin{mslemma}[Key_LowestNeverExported]
    States that no key of level \spthy{'1'} is ever wrapped. 
    Shows that keys of the lowest level are not extractable.
    \begin{minted}{spthy}
"not Ex d k #i . ExportKey(d,k,'1')@i"
    \end{minted}
    \label{lemma:lowest}
\end{mslemma}

\begin{mslemma}[Key_ImportImpliesExport]
    States that whenever a key is imported, this key must have been exported. 
    \begin{minted}{spthy}
"All d k l #import . ImportKey(d,k,l)@import ==>
     Ex D #export . ExportKey(D,k,l)@export & #export<#import"
    \end{minted}
    \label{lemma:imex}
\end{mslemma}

\begin{mslemma}[Key_UnwrapImpliesWrap]
    States that whenever a key is unwrapped, this key must have been wrapped. 
    Is stronger than \cref{lemma:imex} in the sense 
    that the key used to unwrap must have been used to wrap.
    \begin{minted}{spthy}
"All d wk wl ek el #unwrap . Unwrap(d,wk,wl,ek,el)@unwrap ==>
     Ex D #wrap . Wrap(D,wk,wl,ek,el)@wrap & #wrap<#unwrap"
    \end{minted}
\end{mslemma}

\begin{mslemma}[Key_Migration]
    States that whenever a key is initialized after creation 
    it was wrapped before and unwrapped at initialization.
    \begin{minted}{spthy}
"All d k l #create #init . 
    CreateKey(k,l)@create & InitKey(d,k,l)@init & #create<#init ==> 
        Ex D K L z #export . 
            l=L+z & Wrap(D,K,L,k,l)@export & Unwrap(d,K,L,k,l)@init & 
            #create<#export & #export<#init"
    \end{minted}
\end{mslemma}

\begin{mslemma}[{Key_BoundToDevice[use_induction,reuse]}]
    States that keys of a level less or equal \spthy{'1'+'1'+'1'} 
    are initialized on at most one device. 
    They can eventually be wrapped, 
    but since the wrapping key is not shared they can only be unwrapped on the device which created them.
    \begin{minted}{spthy}
"All d k l #keyCreate . 
     CreateKey(k,l)@keyCreate & InitKey(d,k,l)@keyCreate & 
     ( not Ex z . l=z+'1'+'1'+'1' ) ==>
         ( All D #other . InitKey(D,k,l)@other ==> D=d )"
    \end{minted}
    \label{lemma:bound}
\end{mslemma}

\begin{mslemma}[{Key_UnwrapObeysOrder[reuse,hide_lemma=Key_BoundToDevice]}]~
    %TODO@final:see if weird spacing tricks work
    \\States that whenever a key is unwrapped, 
    the used wrapping key is of lower level. 
    \label{lemma:obey}
    \begin{minted}{spthy}
"All d K L k l #unwrap . Unwrap(d,K,L,k,l)@unwrap ==> Ex z . l=L+z"
    \end{minted}
\end{mslemma}

\begin{mslemma}[Key_PairingTwoDevices]
    Depends on \cref{lemma:bound} and \cref{lemma:obey}.
    States that keys with level \spthy{'1'+'1'+'1'+'1'} 
    can only be unwrapped by devices on which they were created. 
    \label{lemma:pairing}
    \begin{minted}{spthy}
"All d k l #keyInit . 
     InitKey(d,k,l)@keyInit & l='1'+'1'+'1'+'1' ==>
         Ex #keyCreate . CreateKey(k,l)@keyCreate & InitKey(d,k,l)@keyCreate"
    \end{minted}
\end{mslemma}

% vim: set syntax=tex:
