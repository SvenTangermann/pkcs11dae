Sources lemmas are applied to the raw sources computed by \tamarin~
to obtain the refined sources.
They often connect facts to their origin 
and help to sort out multiset rule instances 
which cannot be applied. 
Also rules which do not change the state but increase the knowledge of the adversary 
can be targeted by stating that the adversary learns nothing he did not knew before 
or by stating that the new knowledge has certain properties.

\begin{example}[the old in-out game]
    We allow the adversary to store some messages and retrieve them later on:
    \begin{minted}{spthy}
        rule Store: [ In( m ) ]--[Stored(m)]->[ Store( m ) ]
        rule Send: [ Store( m ) ]--[Sent(m)]->[ Out( m ) ]
    \end{minted}
    The adversary could conclude arbitrary terms from the \spthy{Out( m )} facts of the second rule.
    By relating the point when some message is sent to the point where it is stored,
    we can show that the adversary already knows each message he can receive:
    \begin{minted}{spthy}
        lemma SentImpliesStored[sources]:
        "All m #i. Sent(m)@i ==> Ex #j. Stored(m)@j & #j<#i"
    \end{minted}
\end{example}

Sources lemmas are proven by induction, 
which means they are assumed to hold for all points in time but the last one \spthy{last}.
This allows one to proof a lemma with help of the lemma itself.
Wellfoundedness follows from the fact that if the lemma would not hold, 
there would be a first point in time where the lemma is contradicted. 
We call this first point in time \spthy{last} which concludes the proof.
\par
Proofs using the raw sources tend to be more complicated than those using refined sources: 
Often the possible sources can be cut by half using an adequate sources lemma. 
%it is not seldom to be able to cut the possible sources by half using a good sources lemma.
Since we can show such relations in many cases for more than one fact 
and often these lemmas help to show that other lemmas of that type hold, 
in most cases one great sources lemma 
~--~a conjunction of the smaller ones~--~
is used.

\begin{mslemma}[{origin[sources]}]
%In \spthy{lemma origin[sources]} 
Here we connect the decryption of a message under a key on some device 
to the point where the adversary knew the message. 
This allows us to conclude that the adversary learns nothing from the message 
what he did not knew before.
\par
Furthermore we connect the initialization of a key 
to the creation of this key 
or state that the adversary must have known the key before. 
This allows us to conclude that keys are either \spthy{Fr/1} facts 
or already known to the adversary. 
In both cases the adversary cannot learn new terms by decomposing the key
~--~of course he can use keys to decrypt messages, 
but e.g. \spthy{fst(k)} will not reduce to some new term because 
he either knew the key before and could have done the same earlier
or the key was created on a device, meaning it is a fresh name.
\begin{minted}{spthy}
" ( All m #decrypt . Decrypt(m)@decrypt ==> 
      ( Ex #mKU . KU(m)@mKU & #mKU<#decrypt ) ) 
& ( All d k l #keyImport . ImportKey(d,k,l)@keyImport ==> 
      ( Ex #keyCreate . CreateKey(k,l)@keyCreate  & #keyCreate<#keyImport ) 
    | ( Ex #keyKU . KU(k)@keyKU  & #keyKU<#keyImport ) )"
\end{minted}
\end{mslemma}
\par

%\cref{fig:source} gives an example for an impossible source: 
%When some ciphertext sent by the adversary gets decrypted, 
%\tamarin~cannot infer enough information about that ciphertext 
%to exclude the possibility of the decryption being the first component of some other term. 
%This source does not appear in the refined sources since 
%%TODO: better formulation
%\spthy{lemma origin[sources]} sorts it out. 
%\par
%% TODO: do we need this? looks mostly complicated
%\begin{figure}[htb]
    %\centering
    %\includegraphics[scale=0.7]{Figures/impossibleSource}
    %\caption{A partial deconstruction (simplified)}
    %\label{fig:source}
%\end{figure}

\begin{remark}[What is said and what not]
    Later we want to show that the adversary is not able 
    to learn any key which was created on any device. 
    For the moment it suffices to show 
    that learning a key gives knowledge of this key and nothing more.
\end{remark}

\begin{remark}[Structure of sources lemmas]
    It is often beneficial to start with lemmas like 
    ``either this term comes from a valid execution of the protocol, 
    implying it has some known structure, 
    or it comes from the adversary, 
    implying he knew the term before''.
\end{remark}
% vim: set syntax=tex:
