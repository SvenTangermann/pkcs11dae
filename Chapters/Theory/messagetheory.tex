The message theory models both knowledge and deduction.
\begin{definition}[Namespace]
    The namespace $\Names$ is a set of countable infinite names $n$.
\end{definition}

Names model fixed keys, messages, constants and nonces. 
Protocol rules often produce output depending on some input. 
To capture this behavior we introduce variables. 

\begin{definition}[Variablespace]
    The variablespace $\Vars$ is a set of countable infinite variables $v$.
\end{definition}

\begin{remark}[Types]
    The canonical view differs between different types of names and variables.
    While public ones are assumed to be known and thus not have to be deduced,
    fresh ones are assumed to be ``unguessable'' 
    and thus only can be deduced using function applications and equations.
    We will see in 
    \cref{rem:typing}
    how typing can be enforced.
\end{remark}

\begin{definition}[Functions]
    A function symbol is a pair $(f,a)$ of a function name $f$ and an arity $a$,
    usually denoted by $f/a$.
    A Signature $\Sig$ is a finite set of function symbols.
\end{definition}

\begin{definition}[Function Application]
    A function application $( (f,a), (t_1..t_a) )$ is a pair of a function symbol and a tuple of terms
    (see \cref{def:term}),
    denoted by $f(t_1..t_a)$.
\end{definition}

\begin{definition}[Term]
    Terms are defined over a signature $\Sig$.
    A term $t$ is a name $n$,
    a variable $v$
    or a function application $f(t_1..t_a)$
    where $f/a \in \Sig$ and $t_1..t_a$ are terms.
    A term is finite if it is a name, a variable or a function application $f(t_1..t_a)$ where $t_1..t_a$ are finite terms.
    A term is ground if it is a name or a function application $f(t_1..t_a)$ where $t_1..t_a$ are ground terms.
    By $\names{t}$ we denote the set of names occurring in $t$,
    and by $\vars{t}$ the set of variables occurring in $t$.
    $\Terms$ is the countable infinite set of finite terms over a given signature.
\label{def:term}
\end{definition}

\begin{example}
    $and(x,true())$ is a finite term which is not ground since it contains the variable $x$ as subterm. 
    On the other hand $true()$ is a ground term.
\end{example}

\begin{definition}[Equation]
    Equations are defined over a signature $\Sig$.
    An equation is a tuple of terms $(L,R)$, usually written as $L = R$.
    An equation is well-formed if $\vars{R} \subseteq \vars{L}$.
\end{definition}

\begin{example}[Deconstruction of pairs]
    The equation 
    $fst(pair(x,y))=x$
    allows the extraction of the first component of a pair.
\end{example}

\begin{definition}[Equational Theory]
    An equational theory $\ET$ is defined over a signature $\Sig$.
    It is a set of well-formed equations over $\Sig$.
\end{definition}

\begin{example}[Asymmetric Signing]
    An asymmetric signature scheme could be modelled by the following signature and equational theory:
    \begin{equation*}
        \Sig = \Set{verify/2,sign/2,pk/1,msg/1,true/0}
    \end{equation*}
    \begin{equation*}
        \ET = \Set{verify(pk(key),sign(key,message)) = true(), msg(sign(key,message)) = message}
    \end{equation*}
\end{example}

\begin{definition}[Context]
    A context $\Ctx$ is a term which is not ground.
\end{definition}

\begin{definition}[Substitution]
    A substitution $\sigma$ is a partial function $\sigma\from \Vars \partto \Terms$.
    We require the domain of the function to be finite.
    %A substitution $\sigma$ is a set of pairs $(v,s)$ where $v$ is a variable and $s$ is a term.
    A substitution $\sigma$ can be applied to a term $t$, denoted by $t\sigma$,
    which results in a term where each variable $v \in \Dom(\sigma)$ is replaced by the term $\sigma(v)$.
    A substitution is grounding for a context if the resulting term is ground.
    %We require each variable $v \in \Vars$ to occur in at most one pair $(v,t) \in \sigma$.
\end{definition}

\begin{example}[Hashing]
    By defining a function symbol $h/1$ we create a context $h(x)$ where $x$ is a variable. 
    By substituting the variable $x$ with the name $n$ we create a term $h(n)$ which is ground.
\end{example}

\begin{definition}[Equality modulo $\ET$]
    $\eqET$ is the reflexive, transitive, context- and substitution-aware closure of $\ET$
    \parencite{FMTASP}.
    It is the smallest equivalence relation fulfilling
    \begin{equation*}
        \forall l=r \in \ET : l\sigma \eqET r\sigma \quad \forall \sigma
    \end{equation*}
    s.t.
    \begin{equation*}
        f(x_1..x_a) \neqET f(y_1..y_a) \implies \exists i \in \Set{1..a} : x_i \neqET y_i
    \end{equation*}
    Informally two terms $s,t$ are equal modulo $\ET$
    if there is a substitution $\sigma$ and an equation $l = r$ s.t.
    $s = l\sigma$ and $t = r\sigma$ (or $t = l\sigma$ and $s = r\sigma$),
    and whenever two terms are equal modulo $\ET$
    and we have two function applications $f \eqET g$,
    we can exchange one of the arguments by $s$ and $t$ without losing equality modulo $\ET$.
\end{definition}

\begin{example}[Symmetric Encryption]
    Let the signature consist of a single function symbol $senc/2$
    and the equational theory contain one equation $senc(k,senc(k,m)) = m$.
    We want to know if $senc(x,y) \eqET senc(z,z)$ holds.
    $x \eqET z$ allows us to apply $senc/2$ to both terms and gives
    $senc(x,senc(x,y) \eqET senc(z,senc(z,z))$ 
    which reduces to 
    $y\eqET z$. 
    If the initial assumption holds, by transitivity also 
    $x\eqET y$ 
    must hold.
    This leads to a contradiction, since we can instantiate $x$ and $y$ by different names.
\end{example}

% vim: set syntax=tex:
