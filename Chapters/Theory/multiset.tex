Message theory alone is perfectly well suited for protocols where little or no state is required,
as it can either be included in the specification of honest parties
or encoded in exchanged messages 
(which could allow additional attacks in the symbolic model if messages are not authenticated, 
but this is a modeling issue).
However, maintaining a global non-monotonic state where, for example, 
a key is deleted 
or a successful login invalidates previous sessions
depicts limitations which require reasonable effort.
This led to complicated proofs and sometimes unacceptable restrictions, 
e.g. on the number of cryptographic keys or sessions.
Some of the main motivations behind symbolic analysis 
~--~automatization of proofs and ease of modeling~--~
can not be accomplished in this setting.

\begin{definition}[Multiset]
    A multiset $S^\#$ over a set $S$ is a function $S^\# \from S \to \mathbb{N}_0$.
    Lifting set operations to multiset operations is mostly routine:
    \begin{gather}
        S +^\# T := \lambda s. S(s) + T(s)\\
        S \cup^\# T := \lambda s. \max(S(s), T(s))\\
        S \cap^\# T := \lambda s. \min(S(s), T(s))\\
        S \setminus^\# T := \lambda s. \max(0, S(s) - T(s))\\
        S \subseteq^\# T := \forall s \in \Dom(T): S(s) \le T(s)
    \end{gather}
    As expected, the empty multiset is given by $\Set{}^\# := \lambda s. 0$.
\end{definition}

\begin{definition}[Multiset Rewrite Rule]
    A multiset rewrite rule $ri$ is a tuple $(l,a,r) \in \Facts^*\times\Actions^*\times\Facts^*$
    where $ri$ is the name of the rule, $l$ are the premises, $r$ the conclusions and $a$ the actions.
    It is often written $ri \colon l\msrewrite[a]r$.
    If $a$ is empty, sometimes
    $ri \colon l \msrewrite[] r$ 
    is used as a shorthand for 
    $ri \colon l\msrewrite[\Set{}^\#]r$. 
    Substitutions can be applied to rules, leading to possibly different rule instances.
    We call a rule well-formed if for each substitution the set of variables in $r$ and $a$
    are a subset of the set of variables in $l$.
    Later on this ensures names to be introduced by distinct rules.
    \label{def:multisetRewriteRule}
\end{definition}

\begin{example}[Malformed rule]
    The following rule is malformed since we can instantiate $x$ by $<y,z>$.
    Now the variable $z$, which occurs in $r$, does not occur in $l$ since $fst(<y,z>)$ reduces to $y$.
    \begin{equation*}
        \mathtt{fst2snd} \colon [F(fst(x))] \msrewrite[] [F(snd(x))]
    \end{equation*}
\end{example}

    %\tamarin~indeed has distinguished sorts for fresh and public names. 
    Meier gives a formal definition of the well-formedness condition used by \tamarin~\cite[p. 77, MSR1--3]{AutoVerif}.
    Since we did some simplifications like omitting sorts of variables our well-formedness condition is slightly different. 
    While it is implied by the one Meier gives (not the other way around), 
    the models presented in this paper pass \tamarin s well-formedness checks and thus adhere to Meier's condition.

\begin{example}[Counting]
    \begin{equation*}
        \mathtt{count} \colon [Counter(n)] \msrewrite[] [Couter(inc(n))]
    \end{equation*}
    models the incrementation of a counter where $inc/1$ is a unary function.
    \label{ex:counting1}
\end{example}

\begin{definition}[Multiset Rewriting System]
    A multiset rewriting system $\MSR$ is a set of multiset rewrite rules.
\end{definition}

\begin{example}[Counting revisited]
    \begin{gather*}
        \mathtt{start} \colon [] \msrewrite[] [Couter('0')]\\
        \mathtt{count} \colon [Counter(n)] \msrewrite[] [Couter(inc(n))]
    \end{gather*}
    allows our counter to actually do something, 
    since the premise of $count$ cannot be met without $start$.
    \label{ex:counting2}
\end{example}

\begin{remark}[Including persistent facts]
    To transform a linear fact $F/a$ to a persistent fact $!F/a$,
    %modify each multiset rewrite rule $ri=(l,a,r)$ s.t. $l$ and $r$ contain each $F(x_1..x_a)$ at most once.
    %Furthermore ensure that the fact is included in $r$ if it is included in $l$.
    modify each multiset rewrite rule $ri=(l,a,r)$ s.t. $F(x_1..x_a)$ is included in $r$ if it is included in $l$. 
    Furthermore ensure that the fact occurs at most once in each $l$ and $r$.
    \label{rem:persistence}
\end{remark}

\begin{definition}[Origin of names]
    We define two special multiset rewrite rules:
    \begin{gather}
        \mathtt{fresh} \colon [] \msrewrite[Name(\fr x)] [Fr(\fr x)]\\
        \mathtt{public} \colon [] \msrewrite[Name(\$x)] [!Pub(\$x), !K(\$x)]
    \end{gather}
    These two rules are not well-formed according to \cref{def:multisetRewriteRule}.
\end{definition}

\begin{remark}[Typing]
    A multiset rewriting system $\MSR$ 
    can enforce typing by requiring $Fr/1$ and $Pub/1$ facts.
    The prefix $\fr$ indicates freshness,
    the prefix $\$$ marks public variables and names.
    \label{rem:typing}
\end{remark}

\begin{remark}[Origin of names]
    In \cref{ex:counting2} we introduced a rule which creates the $Counter('0')$ fact 
    without $'0'$ appearing in the premise.
    Whenever this happens 
    (the appearance of a variable $x$ or a name $n$ in $r$ which does not occur in $l$) 
    we implicitly assume we had a $Pub(x)$ or a $Pub(n)$ premise.
\end{remark}

\begin{definition}[Deduction by the Adversary]
    For each equation $s=t \in \ET$ we define a rule
    \begin{equation}
        \mathtt{s=t} \colon [!K(s)] \msrewrite[] [!K(t)]
    \end{equation}
    and for each function symbol $f/a$ in $\Sigma$ we define a rule
    \begin{equation}
        \mathtt{f} \colon [!K(x_i) \forall i \in \Set{1..a}] \msrewrite[] [!K(f(x_1..x_a))]
    \end{equation}
    Furthermore we allow the adversary to use fresh names:
    \begin{equation}
        \mathtt{adv} \colon [Fr(\fr x)] \msrewrite[Adv(\fr x)] [!K(\fr x)]\\
    \end{equation}
\end{definition}

\begin{definition}[Interaction with the Adversary]
    We define two special multiset rewrite rules:
    \begin{gather}
        \mathtt{out} \colon [Out(x)] \msrewrite[] [!K(x)]\\
        in \colon [!K(x)] \msrewrite[] [In(x)]
    \end{gather}
\end{definition}
Messages sent to the network are modeled by $Out/1$ facts which can be transformed to $!K/1$ facts.
Messages received from the network are modeled by $In/1$ facts which can be created by $!K/1$ facts.
\begin{definition}[Well-formedness of a multiset rewriting system]
    A multiset rewriting system is well-formed if all multiset rewrite rules are well-formed and 
    no multiset rewrite rule has 
    $In$, $Fr$ or $Pub$ facts in their conclusion or 
    $Out$ facts in their premise. 
\end{definition}

% vim: set syntax=tex:
