In Symbolic Analysis messages are modeled by terms
which consist of constants, variables and function applications to subterms.
Functions are given by a signature and a set of equations usually called equational theory.
Function applications represent the construction of new terms from known terms.
Equations represent the deconstruction of terms.
They are used to model cryptographic primitives,
e.g. the decryption of an encryption yields the original message (correctness).
\begin{example}[Pairing]
    We create the term \spthy{pair(x,y)} by application of the function \spthy{pair/2} to some terms \spthy{x,y}.
\end{example}
\begin{remark}[notation of pairs]
    Pairs \spthy{pair(x,y)} are generally referred as \spthy{<x,y>}.
    Triples \spthy{<x,<y,z>>} are generally referred as \spthy{<x,y,z>}.
    n-tuples are treated similar.
    Note that \spthy{<x,y,z>}$\neq$\spthy{<<x,y>,z>}.
\end{remark}
\begin{remark}[Multisets as names]
    Later on multisets will be used to model natural numbers. 
    For this purpose it is sufficient to encode them as pairs. 
    \spthy{<'1'>} will be the lowest number, \spthy{<'1','1'>} the next and so on. 
    This way the message theory can be kept simple. 
    Note that this encoding works well for multisets containing one single name, 
    if and how it works for multisets with more (and possibly infinite many) names is not tackled here.
\end{remark}

\begin{example}[Deconstruction]
    Using the equation \spthy{inv(inv(z)) = z} and pattern matching \spthy{pair(x,y)} with \spthy{z}, we can reduce the term \spthy{inv(inv(pair(x,y)))} to \spthy{pair(x,y)}.
\end{example}
\begin{example}[Hashing]
    By defining a function \spthy{h/1} but no equations,
    we model a hash function where the input can not be deduced from the output.
    On the other hand, for some known values \spthy{x,y} it is easy to check if \spthy{y} is the hash of \spthy{x}:
    Apply \spthy{h/1} to \spthy{x} and see if the terms are the same, meaning \spthy{h(x) = y}.
\end{example}
Facts model parts of the state of a protocol. 
Just like functions, they consist of terms and are defined by a signature. 
Since state is mutable and non-monotonic, facts can be created and consumed.
\begin{example}[SND-ACK]
    A client sends a message \spthy{m} and creates the unary fact \spthy{Sent(m)}.
    The server answers by sending \spthy{ack(m)}, using a unary function \spthy{ack/1}.
    The client replaces the \spthy{Sent(m)} fact by an \spthy{Acknowledged(m)} fact.
    \label{ex:SND-ACK}
\end{example}
Events indicate the state of a protocol. 
Just like functions and facts, they consist of terms and are defined by a signature. 
Many properties can be expressed by formulas over events. 
\begin{example}[Reachability]
    By including a 0-ary event \spthy{Reachable/0} in the specification of a protocol
    we can check whether the protocol is executable.
\end{example}
\begin{example}[Correspondence]
    By including two unary events \spthy{Start/1} and \spthy{Stop/1} in the specification of a protocol
    we can check whether each event \spthy{Stop(x)} is preceded by an event \spthy{Start(x)}.
\end{example}
The global state could be modeled using a set of facts.
But there are some limitations and peculiarities like sending the same message twice in 
\cref{ex:SND-ACK}.
While solutions exist~--~one could include unique terms in the \spthy{Sent/1} fact in the example~--~
it seems more natural to model a concrete state using a multiset of facts.
Multisets are often referred to as ``bags'' where elements can occur more than once.
\begin{example}[SND-ACK revisited]
    A client sends the same message twice, creating two \spthy{Sent(m)} facts.
    The server answers by sending \spthy{ack(m)}.
    The client replaces one \spthy{Sent(m)} fact by an \spthy{Acknowledged(m)} fact.
    The other \spthy{Sent(m)} fact remains intact until another \spthy{ack(m)} is received.
    \label{ex:SND-ACK-rev}
\end{example}
In 
\cref{ex:SND-ACK} and \cref{ex:SND-ACK-rev}
we assumed the possibility to create and consume facts.
This is achieved by using multiset rewrite rules which can be applied to a state if the premises are met,
resulting in a new state which is the old state minus the premises plus the conclusions.
\par
If state and honest parties can be modeled by multisets, 
can we also model the behavior of an antagonistic environment
~--~commonly referred to as the attacker or adversary~--~
in these terms?
The short answer is yes, but first we need some understanding of what an adversary is.
The most common model is the Dolev-Yao style adversary which has complete control over the network,
meaning he can read, delay or drop all exchanged messages, 
apply functions and equations, 
and send its own messages over the network
\parencite{DolevYao}.
\par
To capture this informal definition we define attacker knowledge as a unary fact,
let all messages sent to the network create such facts 
and require all messages received from the network to be created by these facts.
Furthermore we allow function and equation application to create new attacker knowledge facts.
Since knowledge is inexhaustible these facts are not consumed when they are used,
meaning an attacker can use one message multiple times.

\begin{example}[Communication]
    A client wants to send a message to a server.
    Instead of directly creating a \spthy{Server(m)} fact,
    he creates an \spthy{Out(m)} fact which increases the knowledge of the attacker.
    The attacker can eventually create an \spthy{In(m)} fact 
    which the server transforms to a \spthy{Server(m)} fact.
\end{example}

% vim: set syntax=tex:
