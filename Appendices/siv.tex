\chapter{Using SIV mode of operation}
One can expect users to provide unique initialization vectors. 
This approach failed over and over the last decades. 
Another approach is to achieve initialization vector uniqueness independent of the user input. 
In our model the device counter guarantees this. 
A third approach is to eliminate the need of such a vector. 
The synthetic initialization vector construction \cite{DaeSiv}, short SIV, 
does exactly this. 
\par
Here no initialization vector has to be provided 
because it is computed as a hash of the header (associated data) and the message. 
When a ciphertext gets decrypted later on this hash can be recomputed, 
providing authenticity of header and message. 
The usage of a cryptographic hash function ensures the internally used initialization vector to be different for different messages and/or headers. 
For full details refer to the paper mentioned above. 
Since it has a different signature and other properties than, for example, GCM or CCM, 
soundness results cannot be used directly for SIV. 
\par
%\begin{figure}
    %\centering
    %\input{Tikz/newsiv}
    %\caption{Schematic of the SIV construction}
    %\label{fig:siv}
%\end{figure}
\begin{figure}[ht]
    \centering
    \begin{minipage}{0.5\textwidth}
        \centering
        \input{Tikz/newsiv}
        \captionof{figure}{Schematic of the SIV encryption}
        \label{fig:siv}
    \end{minipage}%
    \begin{minipage}{0.5\textwidth}
        \centering
        \input{Tikz/newsivdec}
        \caption{Schematic of the SIV decryption}
        \label{fig:sivdec}
    \end{minipage}%
\end{figure}
What one can do is to wrap it by a simple layer resulting in the same signature and, 
as a consequence, the same soundness results. 
\cref{fig:hack} provides such a construction. 
Here we simply concatenate the initialization vector and the header, 
denoted by \texttt{iv||h}. 
Dax gives justification for the soundness of this construction \cite{adax}.
\par
Since we want to guarantee the used initialization vector 
(named \texttt{siv} in \cref{fig:siv}) to be unique 
we simply fix the \texttt{iv} input to the empty string 
and include the initialization vector (device id and counter) in \texttt{h}. 
Now \texttt{iv||h} is the same as \texttt{h} 
which allows us to omit the wrap layer and directly use the SIV mode of operation. 
\begin{remark}
    Note that \texttt{iv} has a fixed size since all components have fixed sizes. 
\end{remark}

\begin{figure}[ht]
    \centering
    \input{Tikz/newhack}
    \caption{Schematic of the wrap layer}
    \label{fig:hack}
\end{figure}

The difference between the original model and the version for SIV mode are marginal, 
see \cref{sec:siv} for a patch which can be applied by invoking 

\begin{minted}{sh}
    $ patch -o siv.spthy gcm.spthy siv.diff
\end{minted} 

The lemmas and the oracle still work in the patched model.
\par
The paper mentioned above introduces the term \emph{Misuse-Resistant Authenticated Encryption} 
and provides a generic construction to fulfill this security notion. 
Our wrapping layer matches the generic construction~--~or would match, 
if we would have placed the triple \spthy{<el,device,ctr>} in the header and not the initialization vector. 
The only difference between these two possibilities is that the second one needs further adjustments in the model since \spthy{lemma IV_Uniqueness} would be violated by \spthy{iv = epsilon()}. 
The effect~--~using \spthy{<el,device,ctr>} as header input for SIV~--~is the same.
