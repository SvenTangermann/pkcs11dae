# -*- coding: utf-8 -*-
"""
    pygments.lexers.spthy
    ~~~~~~~~~~~~~~~~~~~~~~~

    Lexer for the tamarin-prover.
    Intended to be included in pygments.lexers.theorem

    :copyright: Copyright 2018 by Sven Tangermann <SvenTangermann@freenet.de>.
    :license: BSD, see LICENSE for details.
"""

import re

from pygments.lexer import RegexLexer, default, words, include, bygroups
from pygments.token import Text, Comment, Operator, Keyword, Name, String, \
    Number, Punctuation, Generic, Whitespace

__all__ = ['SpthyLexer']


class SpthyLexer(RegexLexer):
    """
    For the `Tamarin <https://tamarin-prover.github.io//>`_
    proof assistant.

    mainly stateless
    advantage: snippets can be lexed
    disadvantage: no differentiation between facts and actions possible
    disadvantage: differentiation between functions and facts/actions done by first character
    recommended style: Spathi

    .. versionadded:: TODO
    """

    name='Spthy'
    aliases=['spthy','tamarin']
    filenames=['*.spthy']
    flags = re.MULTILINE | re.DOTALL

    RESERVED = ('theory', 'begin', 'end')
    NAMESPACE = ('builtins', 'functions', 'equations')
    DECLARATIONS = ('rule ', 'lemma', 'diffLemma', 'axiom', 'restriction')
    LET = ('let', 'in')
    TRACES = ('exists-trace', 'all-traces','rule-equivalence')
    ATTRIBUTES = ('reuse', 'use_induction', 'typing', 'sources', 'left', 'right', 'hide_lemma', 'use_lemma', 'color', 'colour')
    PROOFS = ('induction', 'case', 'qed', 'next', 'by', 'simplify', 'solve', 'sorry', 'step')
    STEPS = ('solve', 'step')
    LOGIC = ('|', u'∥', u'∨', '&', '∧', 'not', u'¬', '<', '>', '=', '!=')
    RELATION = (u'∀', u'∃', '==>', u'⇒', '<=>', u'⇔', '.', '~~>')
    QUANTIFIER = ('All', 'Ex')
    PUNCTUATION = ('+', '^', '*', ',', ':')
    MULTISET = ('[', ']')
    ACTIONSET = ('--[', ']->', '-->')
    PARANTHESES = ('(', ')')
    CONSTANTS = ('T', u'⊤', 'F', u'⊥')
    BUILTINS = ('multiset', 'bilinear-pairing', 'diffie-hellman', 'hashing', 'symmetric-encryption', 'asymmetric-encryption', 'signing')
    STDFUNCTIONS = ('aenc', 'adec', 'pk', 'senc', 'sdec', 'sign', 'verify', 'h', 'pair', 'diff')
    STDFACTS = ('K', 'KU', 'KD', 'In', 'Out', 'Fr')
    PREPROC = ('#ifdef', '#endif')

    # wrapper for words(), defaults to require words to end
    def token(ls, p='', s=r'\b'):
        return words(ls, prefix=p, suffix=s)

    tokens = {
        'comment': [ # a possibly nested multiline comment
            (r'[^*/]', Comment.Multiline),
            (r'/\*', Comment.Multiline, '#push'), # nested
            (r'\*/', Comment.Multiline, '#pop'), # delimiter
            (r'[*/]', Comment.Multiline),
        ],
        'annotation': [ # a multiline annotation
            (r'[^*}]', Comment.Special),
            (r'\*\}', Comment.Special, '#pop'), #delimiter
            (r'[*}]', Comment.Special),
        ],
        'string': [ # a public name
            (r'[^\']+', String.Single),
            (r'\'', String.Single, '#pop'), # delimiter
        ],
        'root': [
            (r'\s+', Whitespace),
            (r'//.*?\n', Comment.Single),
            (r'/\*', Comment.Multiline, 'comment'), # start a (possible nested) comment
            (r'[a-zA-Z]\w*\s*{\*', Comment.Special, 'annotation'), # start an annotation
            (token(RESERVED+LET), Keyword.Reserved),
            (token(NAMESPACE), Keyword.Namespace),
            (token(DECLARATIONS), Keyword.Declaration, 'lemma'),
            (token(TRACES), Keyword.Type),
            (token(STEPS,s=r'\('), Keyword.Pseudo, 'proof'),
            (token(PROOFS), Keyword.Pseudo),
            (words(RELATION),Operator.Word),
            (token(QUANTIFIER),Operator.Word),
            (words(LOGIC),Operator),
            (words(PUNCTUATION+ACTIONSET+MULTISET+PARANTHESES),Punctuation),
            (token(CONSTANTS), Keyword.Constant),
            (token(BUILTINS), Name.Namespace),
            (token(PREPROC), Comment.Preproc),
            (token(STDFACTS,s=r'\s*\('), Name.Builtin,'stdfact'), # built-in fact
            (token(STDFACTS,s=r'(\s*/\s*)(\d+\b)'), bygroups(Name.Builtin,Name.Other,Number.Integer)), # built-in fact signature
            (token(STDFACTS,p=r'!\s*',s=r'\s*\('), Name.Builtin.Pseudo,'stdpermfact'), # built-in persistent fact
            (r'(!KU|!K|!KD)(\s*/\s*)(\d+\b)', bygroups(Name.Builtin.Pseudo,Name.Other,Number.Integer)), # built-in persistent fact signature
            (r'[A-Z]\w*\s*\(', Name, 'fact'), # either fact or action
            (r'([A-Z]\w*\s*)(/)(\d+\b)', bygroups(Name,Name.Other,Number.Integer)), # either fact or action signature
            (r'!\s*[A-Z]\w*\s*\(', Name.Constant, 'permfact'), # persistent fact
            (r'(!\s*[A-Z]\w*)(\s*/\s*)(\d+\b)', bygroups(Name.Constant,Name.Other,Number.Integer)), # persistent fact signature
            (token(STDFUNCTIONS,s='\s*\('), Name.Function.Magic, 'stdfunction'), # stdfunction application
            (r'[a-z]\w*\s*\(', Name.Function, 'function'), # function application
            (token(STDFUNCTIONS,s='\s*{'), Name.Function.Magic, 'sstdfunction'), # binary stdfunction application
            (r'[a-z]\w*\s*{', Name.Function, 'sfunction'), # binary function application
            (r'([a-z]\w*)(\s*/\s*)(\d+\b)', bygroups(Name.Function,Name.Other,Number.Integer)),         # function signature
            (r'\d+\b', Number.Integer),
            ('"', Comment), # lemma start/stop
            (r'\'', String.Single, 'string'), # name
            (r'[a-zA-Z]\w*\b', Name.Variable), # untyped names
            (r'~\s*[a-zA-Z]\w*\b', Name.Variable.Magic), # fresh names
            (r'\$\s*[a-zA-Z]\w*\b', Name.Variable.Global), # public names
            (r'(@|@\s*#|#)\s*[a-zA-Z]\w*\b', Name.Variable.Instance), # timepoints
        ],
        'fact': [ # a fact term
            (r'\)', Name, '#pop'),
            include('root'),
        ],
        'permfact': [ # a permanent fact term
            (r'\)', Name.Constant, '#pop'),
            include('root'),
        ],
        'stdfact': [ # a builtin fact term
            (r'\)', Name.Builtin, '#pop'),
            include('root'),
        ],
        'stdpermfact': [ # a builtin permanent fact term
            (r'\)', Name.Builtin.Pseudo, '#pop'),
            include('root'),
        ],
        'function': [ # a function term
            (r'\)', Name.Function, '#pop'),
            include('root'),
        ],
        'stdfunction': [ # a builtin function term
            (r'\)', Name.Function.Magic, '#pop'),
            include('root'),
        ],
        'sfunction': [ # a function term
            (r'}', Name.Function, '#pop'),
            include('root'),
        ],
        'sstdfunction': [ # a builtin function term
            (r'}', Name.Function.Magic, '#pop'),
            include('root'),
        ],
        'lemma': [ # a lemma name and possibly attributes
            (r'\[', Name.Declaration, 'lemma_attribute'),
            (r'\:', Punctuation, '#pop'), # delimiter
            (r'\s*\w+\s*', Name.Declaration),
        ],
        'lemma_attribute': [ # a list of attributes, possibly with parameters
            (words(ATTRIBUTES), Name.Attribute),
            (r'\]', Name.Declaration, '#pop'), # delimiter
            include('root'), # parse names
        ],
        'proof': [ # a proof step
            (r'\)', Keyword.Pseudo, '#pop'),
            include('root'),
        ],
    }

