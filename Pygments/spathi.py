# -*- coding: utf-8 -*-
"""
    pygments.styles.spathi
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Spathi by Sven Tangermann

    A colorful, bright style created for the Tamarin prover (https://tamarin-prover.github.io/)

    Code based on Lovelace by Miikka Salminen (https://github.com/miikkas)

    :license: BSD, see LICENSE for details.
"""

from pygments.style import Style
from pygments.token import Keyword, Name, Comment, String, Error, \
    Number, Operator, Punctuation, Generic, Whitespace


class SpathiStyle(Style):
    _LIGHT_GREEN =  '#90D000'
    _GREEN =        '#728E00'
    _AZURE =        '#0099FF'
    _TURQUOISE =    '#009999'
    _CYAN =         '#289870'
    _BLUE_CYAN =    '#287088'
    _PURPLE =       '#800080'
    _ORANGE=        '#FF6600'
    _DARK_ORANGE =  '#ce5c00'
    _YELLORANGE =   '#EEAA22'
    _GRASS_GREEN =  '#6ab825'
    _RED =          '#CC3300'
    _DARK_RED =     '#882200'
    _LIGHT_RED =    '#FF0000'
    _LIGHT_GRAY =   '#888888'
    _GRAY =         '#555555'
    _DARK_GRAY =    '#000000'
    _DARK_GREEN =   '#00aa00'
    _BG =           ' bg:#999999'
    styles = {
        Keyword:                _GREEN,                 #
        Keyword.Constant:       ' bold '+_DARK_RED,     # T, F
        Keyword.Namespace:      ' bold '+_ORANGE,       # builtins, functions, equations
        Keyword.Pseudo:         ' bg:'+_LIGHT_GREEN,    # qed etc.
        Keyword.Type:           _ORANGE,                # exists-trace, all-traces
        # Keyword.Reserved:       ' '                   # theory begin end let in
        # Keyword.Declaration:    ' ',                  # rule, lemma, restriction
        Name.Declaration:       _ORANGE,                # name of rules, lemmas, restrictions
        Name:                   _AZURE,                 # Fact
        Name.Constant:          ' bold ',               # !Fact
        Name.Builtin:           _CYAN,                  # Pub, Fr, In
        Name.Builtin.Pseudo:    ' bold ',               # !K
        Name.Variable:          _ORANGE,                # term
        Name.Variable.Global:   _DARK_ORANGE,           # $var
        Name.Variable.Magic:    _YELLORANGE,            # ~var
        Name.Variable.Instance: ' italic '+_BLUE_CYAN,  # @i, #i
        Name.Function:          _GRASS_GREEN,           # fun()
        Name.Function.Magic:    ' bold ',               # senc()
        # Name.Attribute:      _NAME_GREEN,             # reuse, sources
        # Name.Other:                                   # / in signatures
        Name.Class:             ' italic ',             # Actions
        # Name.Decorator:      _CLS_CYAN,
        # Name.Entity:         _ESCAPE_LIME,
        # Name.Exception:      _EXCEPT_YELLOW,
        # Name.Label:          _LABEL_CYAN,
         Name.Namespace:        ' bold '+_AZURE,        # multiset, signing, hashing
        # Name.Tag:            _KW_BLUE,

        # Whitespace:             '#a89028',
        Comment:                ' italic '+_LIGHT_GRAY,
        Comment.Hashbang:       ' noinherit '+_CYAN,
        Comment.Multiline:      ' noitalic '+_GRAY,    # comments
        Comment.Special:        _GRAY,                  # annotations
        Comment.Preproc:        'noitalic '+_CYAN,      # ifdef, ifndef

        Operator:               ' bold '+_GRAY,         # & | < > = != not
        Operator.Word:          ' bold '+_RED,          # All Ex . ==> <=>
        Punctuation:            _DARK_GRAY,             # < > , : + ^ ( )

        String:                 _RED,                   # '0'
        # String.Affix:        '#444444',
        # String.Char:         _OW_PURPLE,
        # String.Delimiter:    _DOC_ORANGE,
        # String.Doc:          'italic '+_DOC_ORANGE,
        # String.Escape:       _ESCAPE_LIME,
        # String.Interpol:     'underline',
        # String.Other:        _OW_PURPLE,
        # String.Regex:        _OW_PURPLE,

        Number:                 _LIGHT_RED,             # 5 in fun/5

        # Generic.Deleted:     '#c02828',
        # Generic.Emph:        'italic',
        # Generic.Error:       '#c02828',
        # Generic.Heading:     '#666666',
        # Generic.Subheading:  '#444444',
        # Generic.Inserted:    _NAME_GREEN,
        # Generic.Output:      '#666666',
        # Generic.Prompt:      '#444444',
        # Generic.Strong:      'bold',
        # Generic.Traceback:   _KW_BLUE,

        # Error:               'bg:'+_RED,
    }
