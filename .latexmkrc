$pdf_mode = 1;
$bib_program = 'biber';
$bibtex_use = 2;
@default_files = ('main.tex');
$latex = 'latex -interaction=nonstopmode -shell-escape';
$pdflatex = 'pdflatex -synctex=1 -interaction=nonstopmode -shell-escape';
